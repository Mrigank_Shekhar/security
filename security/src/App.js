import React, { Component } from 'react';
import Security from './security/security';

export default class App extends Component {
  render() {
    return (
      <div>
        <Security/>
      </div>
    );
  }
}