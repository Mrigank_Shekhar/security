import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ThirdtHead from './thirdHead/thirdLine';
import FourthHead from './fourthHead/fourthLine';
import FifthHead from './fifthHead/fifthLine';

export default class Routed extends Component {
    render(){
        return(
            <div>
                <div> <ThirdtHead/> </div>
                <div  className='container'> <FourthHead/> </div>
                <div> <FifthHead/> </div>
            </div>
        )
    }
}