import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './fourthLine.css';
import { MDBBtnGroup, MDBBtn} from "mdbreact";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWifi, faUmbrella, faUpload,} from '@fortawesome/free-solid-svg-icons';

export default class FourthHead extends Component {
    render(){
        return(
            <div>
                    <h3 className="text-center" id='text'>SECURITY DEPARTMENTS</h3>
                    <hr id='hr'/>
                <div className='row'>
                    <div className="card w-40 p-5 col-md-4 block-example border border-light ">
                    <div className="card-body">
                        <h5 className="card-title">NETWORK SECURITY</h5>
                        <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <MDBBtnGroup>
                            <MDBBtn color="info"><FontAwesomeIcon icon={faWifi} size='lg'/></MDBBtn>
                        </MDBBtnGroup>
                    </div>
                    </div>

                    <div className="card w-40 p-5 col-md-4 block-example border border-light ">
                    <div className="card-body">
                        <h5 className="card-title">DEFENCE TRAINING</h5>
                        <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <MDBBtnGroup>
                            <MDBBtn color="info"><FontAwesomeIcon icon={faUmbrella} size='lg'/></MDBBtn>
                        </MDBBtnGroup>
                    </div>
                    </div>
                    <div className="card w-40 p-5 col-md-4 block-example border border-light ">
                    <div className="card-body">
                        <h5 className="card-title">GUARD HOUSE</h5>
                        <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <MDBBtnGroup>
                            <MDBBtn color="info"><FontAwesomeIcon icon={faUpload} size='lg'/></MDBBtn>
                        </MDBBtnGroup>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}