import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MDBBtnGroup, MDBBtn,MDBRow,MDBPagination, MDBPageItem, MDBPageNav, MDBCol} from "mdbreact";
import 'react-widgets/dist/css/react-widgets.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPenNib, faArrowRight} from '@fortawesome/free-solid-svg-icons';
import image4 from './image4.jpg';
import image5 from './image5.jpg';
import image6 from './image6.jpg';
import './fifthLine.css';

export default class FifthHead extends Component {
    render(){
        return(
        <div className='row mt-5 container'>
            <div className='col-md-8'>
            <div className="card w-60 block-example border border-light">
                    <div className="card-body">
                        <h5 className="card-title"><FontAwesomeIcon icon={faPenNib} size='lg'/> TEMPLATE STICKY</h5>
                        <p className="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <MDBBtnGroup>
                            <MDBBtn color="info" >LEARN MORE</MDBBtn>
                            <MDBBtn color="warning" ><FontAwesomeIcon icon={faArrowRight} size='lg'/></MDBBtn>
                        </MDBBtnGroup>
                    </div>
                        <div className="card w-60 block-example border border-light">
                            <div className='card-body'>
                            <div className="row">
                                <div className="col-md-5 ">
                                    <img className='cardImage' src={image4} alt="card-profilepicture"/>
                                </div>
                                <div className="card-block px-2 col-md-7">
                                    <h4 className="card-title">GUARD HOUSE</h4>
                                    <hr id='hr1'/>
                                    <p className="card-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <MDBBtnGroup>
                                        <MDBBtn color="info" >READ MORE</MDBBtn>
                                        <MDBBtn color="warning" ><FontAwesomeIcon icon={faArrowRight} size='lg'/></MDBBtn>
                                    </MDBBtnGroup>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div className="card w-60 block-example border border-light">
                            <div className='card-body'>
                            <div className="row">
                                <div className="col-md-5 ">
                                    <img className='cardImage' src={image6} alt="card-profilepicture"/>
                                </div>
                                <div className="card-block px-2 col-md-7">
                                    <h4 className="card-title">DEFENCE TRAINING</h4>
                                    <hr id='hr1'/>
                                    <p className="card-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <MDBBtnGroup>
                                        <MDBBtn color="info" >READ MORE</MDBBtn>
                                        <MDBBtn color="warning" ><FontAwesomeIcon icon={faArrowRight} size='lg'/></MDBBtn>
                                    </MDBBtnGroup>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div className="card w-60 block-example border border-light">
                            <div className='card-body'>
                            <div className="row">
                                <div className="col-md-5 ">
                                    <img className='cardImage' src={image5} alt="card-profilepicture"/>
                                </div>
                                <div className="card-block px-2 col-md-7">
                                    <h4 className="card-title">NETWORK SECURITY</h4>
                                    <hr id='hr1'/>
                                    <p className="card-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <MDBBtnGroup>
                                        <MDBBtn color="info" >READ MORE</MDBBtn>
                                        <MDBBtn color="warning" ><FontAwesomeIcon icon={faArrowRight} size='lg'/></MDBBtn>
                                    </MDBBtnGroup>
                                </div>
                                <div className="card-block mt-5">
                                    <h4 className="card-title">Markup : Title With Special Characters</h4>
                                    <hr id='hr1'/>
                                    <p className="card-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <MDBBtnGroup>
                                        <MDBBtn color="info" >READ MORE</MDBBtn>
                                        <MDBBtn color="warning" ><FontAwesomeIcon icon={faArrowRight} size='lg'/></MDBBtn>
                                    </MDBBtnGroup>
                                </div>
                                <div className="card-block mt-5">
                                    <h4 className="card-title">Markup : Title With Markup</h4>
                                    <hr id='hr1'/>
                                    <p className="card-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <MDBBtnGroup>
                                        <MDBBtn color="info" >READ MORE</MDBBtn>
                                        <MDBBtn color="warning" ><FontAwesomeIcon icon={faArrowRight} size='lg'/></MDBBtn>
                                    </MDBBtnGroup>
                                </div>
                                <div>
                                <MDBRow className='mt-5 text-center'>
                                    <MDBCol>
                                        <MDBPagination circle>
                                        <MDBPageItem disabled>
                                            <MDBPageNav className="page-link">
                                            <span>First</span>
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem disabled>
                                            <MDBPageNav className="page-link" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span className="sr-only">Previous</span>
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem active>
                                            <MDBPageNav className="page-link">
                                            1 <span className="sr-only">(current)</span>
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem>
                                            <MDBPageNav className="page-link">
                                            2
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem>
                                            <MDBPageNav className="page-link">
                                            3
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem>
                                            <MDBPageNav className="page-link">
                                            4
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem>
                                            <MDBPageNav className="page-link">
                                            5
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem>
                                            <MDBPageNav className="page-link">
                                            &raquo;
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        <MDBPageItem>
                                            <MDBPageNav className="page-link">
                                            Last
                                            </MDBPageNav>
                                        </MDBPageItem>
                                        </MDBPagination>
                                    </MDBCol>
                                    </MDBRow>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div className='col-md-4'>
                <div className='block-example border border-light'>
                   <div className='p-2'><input className="form-control" 
                        type="text" 
                        placeholder="Search" 
                        aria-label="Search" />
                    </div>
                    <div className='mt-3 p-2'>
                        <MDBBtnGroup className='btn-block'>
                        <MDBBtn color="info">SEARCH</MDBBtn>
                        </MDBBtnGroup>  
                    </div>
                </div>
                <div className='block-example border border-light mt-4'>
                    <ul>
                        <fieldset>
                        <legend className='text-center'>Archive List</legend>
                        <li className="list-group-item">March 2019</li>
                        <li className="list-group-item">February 2019</li>
                        <li className="list-group-item">January 2019</li>
                        <li className="list-group-item">December 2018</li>
                        <li className="list-group-item">November 2018</li>
                        <li className="list-group-item">October 2018</li>
                        <li className="list-group-item">September 2018</li>
                        </fieldset>
                    </ul>
                </div>
                <div id='cal' className='block-example border border-light'>
                <iframe title='calender' src="https://calendar.google.com/calendar/embed?src=en.indian%23holiday%40group.v.calendar.google.com&ctz=Asia%2FKolkata"></iframe>
                </div>
            </div>
        </div>
        )
    }
}