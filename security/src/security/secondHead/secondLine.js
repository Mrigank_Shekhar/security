import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './secondLine.css';
import {Navbar,Nav,NavDropdown,Form,FormControl,Button} from 'react-bootstrap';
import { BrowserRouter,Route,Switch } from 'react-router-dom';
import {About,Services,Gallery,Pages,News,Contact} from './navbarContent/navContent.js';
import Logo3 from './Logo3.png';
import Routed from '../routedComponent';

export default class SecondtHead extends Component {
    render(){
        return(
            <BrowserRouter>
            <div>
            <Navbar bg="light" expand="lg">
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                    <Nav.Link href="/home"><img src={Logo3} alt='logo'></img></Nav.Link>        
                    <Nav.Link href="/home">Home</Nav.Link>
                    <Nav.Link href="/about">About</Nav.Link>
                    <Nav.Link href="/contactus">Contact Us</Nav.Link>
                    <NavDropdown title="More" id="basic-nav-dropdown">
                        <NavDropdown.Item href="services">Services</NavDropdown.Item>
                        <NavDropdown.Item href="gallery">Gallery</NavDropdown.Item>
                        <NavDropdown.Item href="pages">Pages</NavDropdown.Item>
                        <NavDropdown.Item href="news">News</NavDropdown.Item>
                    </NavDropdown>
                    </Nav>
                    <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>    
            </Navbar>
                <Switch>
                    <Route path="/home" component={Routed} />
                    <Route path="/about" component={About} />
                    <Route path="/contactus" component={Contact} />
                    <Route path="/services" component={Services} />
                    <Route path="/gallery" component={Gallery} />
                    <Route path="/pages" component={Pages} />
                    <Route path="/news" component={News} />
                </Switch>
                { this.props.children }
            </div>
            
            </BrowserRouter>
        )
    }
}