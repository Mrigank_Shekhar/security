import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
//import Logo2 from '../Logo2.png';

class About extends Component {
    render(){
        return(
            <div>
                <h1>We Will Tell You About Us Soon.</h1>
            </div>
        )
    }
}
class Services extends Component {
    render(){
        return(
            <div>
                <h1>We Will Provide Many Services.</h1>
            </div>
        )
    }
}
class Gallery extends Component {
    render(){
        return(
            <div>
                <h1>We Will Show You Image Gallery.</h1>
            </div>
        )
    }
}
class Pages extends Component {
    render(){
        return(
            <div>
                <h1>Pages For Services Or Products.</h1>
            </div>
        )
    }
}
class News extends Component {
    render(){
        return(
            <div>
                <h1>News Related to World and National Security.</h1>
            </div>
        )
    }
}
class Contact extends Component {
    constructor(){
        super()
        this.state={
                    user_name:'',
                    password:''
                }
        this.handleuser_namechange=this.handleuser_namechange.bind(this)
        this.handlepasswordchange=this.handlepasswordchange.bind(this)
        this.handlesubmit=this.handlesubmit.bind(this)
        }
        handleuser_namechange(event){this.setState({user_name:event.target.value})
        console.log(this.state.user_name)
        }
        handlepasswordchange(event){this.setState({password:event.target.value})
            console.log(this.state.password)
        }
        handlesubmit(event){
            event.preventDefault();
            const data = new FormData()
            data.append( 'user_name',this.state.user_name)
            data.append( 'password',this.state.password)
            alert("User Email is :"+this.state.user_name+"and User Password Is:"+this.state.password)
            console.log(data)
            fetch('http://fatafatservice.com/sparezone_application/api/user/login', {
                method:"POST",
                body:data,
                headers:{
                    "Content-Type": "text/html",
                    "Access-Control-Allow-Origin":"*",
                },
                mode: 'no-cors'
              }).then(function(response) {
                console.log(response.status)
                console.log(response.statusText)
                console.log(response.url)
                console.log(response.text)
              })
        }
    render(){
        return(
            <section className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                        <form className='form p-5' encType="multipart/form-data" onSubmit={this.handlesubmit}>
                            <input  type='text'
                                    name='User_name'
                                    value={this.state.user_name}
                                    placeholder="Enter Your Email Id"
                                    className='form-control m-3'
                                    onChange={this.handleuser_namechange}
                            />
                            <input  type='password'
                                    name='password'
                                    value={this.state.password}
                                    placeholder="Password"
                                    className="form-control m-3"
                                    onChange={this.handlepasswordchange}
                            />
                            <input  type='submit'
                                    name='submit'
                                    value='submit'
                                    className='form-control m-3 btn btn-primary'
                            />
                        </form>
                    </div>
                    <div className='col-md-6'>
                        <h4 className='text-center p-5'>Log In To Contact Us.</h4>
                    </div>
                </div>
            </section>
        )
    }
}
export {About,Services,Gallery,Pages,News,Contact}