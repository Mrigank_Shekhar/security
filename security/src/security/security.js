import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import FirstHead from './firstHead/firstLine.js';
import SecondtHead from './secondHead/secondLine.js'
import SixthHead from './sixthHead/sixthLine.js';

export default class Security extends Component {
  render() {
    return (
      <div>
        <div className="container-fluid">
        <FirstHead/>
        </div>
        <div>
        <SecondtHead/>
        </div>
        <div>
          <SixthHead/>
        </div>
      </div>         
    );
  }
}