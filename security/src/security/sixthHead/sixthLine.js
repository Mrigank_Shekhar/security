import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './sixthLine.css';
import { MDBDropdown,MDBDropdownToggle,MDBDropdownMenu,MDBDropdownItem} from "mdbreact";

export default class SixthHead extends Component{
    render(){
        return(
            <section className='div-footer'>
            <div className='container'>
            <div className='row '>
                <div className='col-md-3'>
                    <h5 className='mb-4 mt-4'>Archives List</h5>
                        <p>March 2019</p>
                        <p>Fabruary 2019</p>
                        <p>January 2019</p>
                        <p>December 2019</p>
                        <p>November 2019</p>
                        <p>October 2019</p>
                </div>
                <div className='col-md-3'>
                <MDBDropdown>
                <h5 className='mt-4 mb-4'>Categories Dropdown</h5>
                <MDBDropdownToggle className='mt-4' caret color="warning">
                    Select Category
                </MDBDropdownToggle>
                <MDBDropdownMenu basic>
                    <MDBDropdownItem>Action</MDBDropdownItem>
                    <MDBDropdownItem>Another Action</MDBDropdownItem>
                    <MDBDropdownItem>Something else here</MDBDropdownItem>
                    <MDBDropdownItem>Separated link</MDBDropdownItem>
                </MDBDropdownMenu>
                </MDBDropdown>
                <div>
                    <h5 className='mt-4 mb-4'>Pages</h5>
                    <p>Blog</p>
                    <p>Cart</p>
                    <p>Check Out</p>
                    <p>Front Page</p>
                    <p>My Account</p>
                    <p>Sample Page</p>
                </div>
                </div>
                <div className='col-md-3'>
                <MDBDropdown>
                <h5 className='mt-4 mb-4'>Archives Dropdown</h5>
                <MDBDropdownToggle className='mt-4 mb-4' caret color="warning">
                    Select Month
                </MDBDropdownToggle>
                <MDBDropdownMenu basic>
                    <MDBDropdownItem>January</MDBDropdownItem>
                    <MDBDropdownItem>Fabruary</MDBDropdownItem>
                    <MDBDropdownItem>March</MDBDropdownItem>
                    <MDBDropdownItem>April</MDBDropdownItem>
                    <MDBDropdownItem>May</MDBDropdownItem>
                    <MDBDropdownItem>June</MDBDropdownItem>
                    <MDBDropdownItem>July</MDBDropdownItem>
                    <MDBDropdownItem>August</MDBDropdownItem>
                    <MDBDropdownItem>September</MDBDropdownItem>
                    <MDBDropdownItem>October</MDBDropdownItem>
                    <MDBDropdownItem>November</MDBDropdownItem>
                    <MDBDropdownItem>December</MDBDropdownItem>
                </MDBDropdownMenu>
                </MDBDropdown>
                <div>
                <div id='cal' className='block-example border border-light mt-3'>
                <iframe title='calender' src="https://calendar.google.com/calendar/embed?src=en.indian%23holiday%40group.v.calendar.google.com&ctz=Asia%2FKolkata"></iframe>
                </div>
                </div>
                </div>
                <div className='col-md-3'>
                <h5 className='mb-3 mt-4'>Tag Cloud</h5>
                    <div className='tag'>
                    <a href='/home'>8-Bit</a>
                    <a href='/home'>Alignment</a>
                    <a href='/home'>Articals</a>
                    </div>   
                </div>   
            </div>
           
            
            </div>
            <footer className='p-4 mt-3 border'>
            <div className='text-center'> &copy;2019 All Right Reserved.</div>
            </footer>
            </section>
        )
    }
}