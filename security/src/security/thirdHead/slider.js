import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './thirdLine.css';
import {MDBCarousel,
        MDBCarouselInner,
        MDBCarouselCaption,
        MDBBtnGroup,
        MDBBtn,
        MDBCarouselItem,
        MDBView,
        MDBMask} from "mdbreact";
import image1 from './image1.jpg';
import image2 from './image2.jpg';
import image3 from './image3.jpg';
import image4 from './image4.jpg';

export default class Slider extends Component {
    render(){
        return(
            
        <div>
            <MDBCarousel activeItem={1} length={4} showControls={true} showIndicators={true} className="z-depth-10 block-example border border-info">
                <MDBCarouselInner>
                <MDBCarouselItem itemId="1">
                    <MDBView>
                    <img className="d-block h-30 w-100 slide" src={image1} alt="First slide" />
                    <MDBMask overlay="black-light" />
                    </MDBView>
                </MDBCarouselItem>
                <MDBCarouselCaption>
                        <h3 className="h3-responsive">Security by Obsecurity.
                            Don't leave data or portale computing devices alone and in view.
                        </h3><br/>
                        <p className='p-responsive'>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </p><br/>
                        <MDBBtnGroup>
                            <MDBBtn color="info" >GET SERVICE QUOTE</MDBBtn>
                        </MDBBtnGroup>
                </MDBCarouselCaption>
                <MDBCarouselItem itemId="2">
                    <MDBView>
                    <img className="d-block h-30 w-100 slide" src={image2} alt="Second slide" />
                    <MDBMask overlay="black-strong" />
                    </MDBView>
                </MDBCarouselItem>
                <MDBCarouselItem itemId="3">
                    <MDBView>
                    <img className="d-block h-30 w-100 slide" src={image3} alt="Third slide" />
                    <MDBMask overlay="black-slight" />
                    </MDBView>
                </MDBCarouselItem>
                <MDBCarouselItem itemId="4">
                    <MDBView>
                    <img className="d-block h-30 w-100 slide" src={image4} alt="Fourth slide" />
                    <MDBMask overlay="black-light" />
                    </MDBView>
                </MDBCarouselItem>
                </MDBCarouselInner>
            </MDBCarousel>
        </div>
           
        )
    }
}