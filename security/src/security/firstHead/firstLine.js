import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MDBBtnGroup, MDBBtn} from "mdbreact";
import {faFacebookF,faTwitterSquare,faYoutubeSquare,faGooglePlusSquare, faLinkedinIn} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBiohazard, faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import './firstLine.css';

export default class FirstHead extends Component {
  render() {
    return (
      
        <div className='row'>
          <div id='d1' className='col-lg-3 col-md-3 col-12'>
              <p id='p1' className ='pt-2 mb-0'><FontAwesomeIcon  icon={faBiohazard} />94 Leetan Wallingford,CT 06492</p>
          </div>
          <div className='col-lg-6 col-md-6 text-center col-6 d'>
          <MDBBtnGroup>
            <MDBBtn href='https://www.facebook.com'><FontAwesomeIcon color='white' icon={faFacebookF}/></MDBBtn>
            <MDBBtn><FontAwesomeIcon color='white' icon={faTwitterSquare} /></MDBBtn>
            <MDBBtn><FontAwesomeIcon color='white' icon={faYoutubeSquare} /></MDBBtn>
            <MDBBtn><FontAwesomeIcon color='white' icon={faGooglePlusSquare} /></MDBBtn>
            <MDBBtn><FontAwesomeIcon color='white' icon={faLinkedinIn}/></MDBBtn>
          </MDBBtnGroup>
          </div>
          <div className='col-lg-3 col-md-3 col-6 d'>
            <MDBBtnGroup>
            <MDBBtn color="info">Get Service Quote</MDBBtn>
            <MDBBtn color="warning"><FontAwesomeIcon icon={faLongArrowAltRight} /></MDBBtn>
              
              </MDBBtnGroup>     
          </div>
        </div>
    );
  }
}